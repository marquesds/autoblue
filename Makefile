clean:
	find . -name "*.pyc" -delete
	find . -name "*.pyo" -delete
	rm -f .coverage

runserver:
	python src/manage.py runserver

migrate:
	python src/manage.py migrate

makemigrations:
	python src/manage.py makemigrations

test: clean
	python src/manage.py test
