from io import BytesIO
from PIL import Image
import requests
import pytesseract


def solve_captcha(url):
    if not url.startswith('http'):
        if url.startswith('//'):
            url = 'http:{0}'.format(url)
        else:
            url = 'http://{0}'.format(url)
    try:
        response = requests.get(url)
        img = Image.open(BytesIO(response.content))
        return pytesseract.image_to_string(img)
    except:
        return '00'


def decrypt(url):
    patternsA = {
        '69': 6, '68': 7, '67': 8,
        '66': 9, '6A': 5, '6B': 4,
        '6C': 3, '6D': 2, '6E': 1,
        '6F': 0
    }
    patternsB = {
        '79': 4, '78': 5, '7A': 7,
        '7B': 6, '7C': 1, '7D': 0,
        '7E': 3, '7F': 2, '75': 8,
        '74': 9
    }
    url = url.split('&')[0]
    cipher = url.split('?t=')[1]
    cA = cipher[:2]
    cB = cipher[2:4]
    return '{0}{1}'.format(patternsA.get(cA, 0), patternsB.get(cB, 0))
