# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class GarageItem(scrapy.Item):
    url = scrapy.Field()
    name = scrapy.Field()
    phones = scrapy.Field()
    address = scrapy.Field()
    tags = scrapy.Field()
    expedient = scrapy.Field()
