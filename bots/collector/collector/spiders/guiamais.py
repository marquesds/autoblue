import subprocess
import scrapy
from collector.items import GarageItem
from collector.helpers import decrypt


class GuiaMaisSpider(scrapy.Spider):
    name = 'guiamais'
    allowed_domains = ['guiamais.com.br']
    start_urls = [
        'http://www.guiamais.com.br/encontre?searchbox=true&what=Oficinas+Mec%C3%A2nicas&where=SP',
    ]

    def parse(self, response):
        links = response.xpath('//h2[@class="advTitle"]//a')
        for link in links:
            url = response.urljoin(link.xpath('./@href').extract_first())
            yield scrapy.Request(url, callback=self.parse_garage)
        next_url = response.xpath('//nav[@class="pagination"]/a[contains(text(),"Mais")]/@href')
        if next_url:
            url = response.urljoin(next_url.extract_first())
            yield scrapy.Request(url, callback=self.parse)
        yield {'response': response}

    def parse_garage(self, response):
        item = GarageItem()
        name = response.xpath('//h1/text()').extract_first() or ''
        phones = response.xpath('//p[@class="phone detail"]')
        street = response.xpath('//address[@class="advAddress"]/p/span/a/text()')
        address = response.xpath('//address[@class="advAddress"]/p/span/text()')
        tags = response.xpath('//p[@class="category"]/a/text()')
        expedient_days = response.xpath('//div[@id="scheduleListItens"]/p/strong/text()').extract()
        expedient_hours = response.xpath('//div[@id="scheduleListItens"]/p/span/text()').extract()

        cleaned_phones = []
        for phone in phones:
            number = phone.re(r'(\([0-9]{2}\)\s9?[0-9]{4}\-[0-9]{4})')[0]
            cleaned_phones.append(number)

        expedient = []
        for i, day in enumerate(expedient_days):
            try:
                expedient.append("{0} - {1}".format(day, expedient_hours[i]))
            except:
                pass

        item['url'] = response.request.url
        item['name'] = name.strip()
        item['phones'] = cleaned_phones
        item['address'] = ''.join(street.extract()) + ''.join(address.extract())
        item['tags'] = tags.extract()
        item['expedient'] = ', '.join(expedient)
        yield item
