import subprocess
import scrapy
from collector.items import GarageItem
from collector.helpers import decrypt


class TeleListasSpider(scrapy.Spider):
    name = 'telelistas'
    allowed_domains = ['telelistas.net']
    start_urls = [
        'http://www.telelistas.net/sp/cidade/oficinas+mecanicas',
    ]

    def parse(self, response):
        links = response.xpath('//td[@width="324"]//a')
        for link in links:
            url = response.urljoin(link.xpath('./@href').extract_first())
            yield scrapy.Request(url, callback=self.parse_garage)
        next_url = response.xpath('//td[@width="80" and @align="right" and @valign="middle"]/a[@rel="nofollow"]/@href')
        if next_url:
            url = response.urljoin(next_url.extract_first())
            yield scrapy.Request(url, callback=self.parse)
        yield {'response': response}

    def parse_garage(self, response):
        item = GarageItem()
        name = response.xpath('//h1/text()').extract_first() or ''
        phones = response.xpath('//div[@id="telInfo"]/div/span')
        style = 'margin: 15px 0 0 0px; font:bold 12px Arial, Verdana, Helvetica;'
        address = response.xpath('//div[@style="{0}"]/text()'.format(style))
        tags = response.xpath('//h2/a/text()')
        cleaned_phones = []
        for phone in phones:
            number = phone.re(r'(\([0-9]{2}\)\s9?[0-9]{4}\-[0-9]{2})')[0]
            captcha = phone.xpath('img/@src').extract_first()
            phone_number = number + decrypt(response.urljoin(captcha))
            cleaned_phones.append(phone_number)
        item['url'] = response.request.url
        item['name'] = name.strip()
        item['phones'] = cleaned_phones
        item['address'] = ''.join(address.extract())
        item['tags'] = tags.extract()
        yield item
