from django.conf.urls import url
from django.contrib.auth.views import login, logout
from .views import register

urlpatterns = [
    url(r'^registrar/', register, name='register'),
    url(r'^entrar/', login, {'template_name': 'accounts/login.html'}, name='login'),
    url(r'^sair/', logout, {'next_page': 'accounts:login'}, name='logout'),
]
