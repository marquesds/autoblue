from django.contrib import admin
from .models import Address, Tag


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    search_fields = ('zipcode', 'street', 'district', 'city', 'state')
    ordering = ('zipcode',)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)
