import re
from django.db import models
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _


class Address(models.Model):
    zipcode = models.CharField(
        _('CEP'),
        max_length=9,
        validators=[RegexValidator(regex=re.compile(r'^\d{8}'), message=_('CEP Inválido'))]
    )
    street = models.CharField(_('Logradouro'), max_length=300)
    district = models.CharField(_('Bairro'), max_length=200)
    city = models.CharField(_('Cidade'), max_length=200)
    state = models.CharField(_('Estado'), max_length=2)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.zipcode

    class Meta:
        verbose_name = _('Endereço')
        verbose_name_plural = _('Endereços')


class Tag(models.Model):
    name = models.CharField(_('Nome'), max_length=100, unique=True)
    slug = models.SlugField(_('Slug'), max_length=100)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')
