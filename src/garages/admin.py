from django.contrib import admin
from .models import Garage, ServiceType


@admin.register(Garage)
class GarageAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)


@admin.register(ServiceType)
class ServiceTypeAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)
