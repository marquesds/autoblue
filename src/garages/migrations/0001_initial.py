# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-15 01:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Garage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Nome')),
                ('slug', models.SlugField(max_length=200, verbose_name='Slug')),
                ('description', models.TextField(max_length=5000, verbose_name='Descrição')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('address', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='garages', to='core.Address')),
            ],
            options={
                'verbose_name_plural': 'Mecânicas',
                'verbose_name': 'Mecânica',
            },
        ),
        migrations.CreateModel(
            name='ServiceType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Nome')),
                ('description', models.TextField(max_length=5000, verbose_name='Descrição')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Tipos de Serviço',
                'verbose_name': 'Tipo de Serviço',
            },
        ),
        migrations.AddField(
            model_name='garage',
            name='service_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='garages', to='garages.ServiceType'),
        ),
        migrations.AddField(
            model_name='garage',
            name='tag',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='garages', to='core.Tag'),
        ),
    ]
