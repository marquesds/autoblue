from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import Address, Tag


class ServiceType(models.Model):
    name = models.CharField(_('Nome'), max_length=200)
    description = models.TextField(_('Descrição'), max_length=5000)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Tipo de Serviço')
        verbose_name_plural = _('Tipos de Serviço')


class Garage(models.Model):
    name = models.CharField(_('Nome'), max_length=200)
    slug = models.SlugField(_('Slug'), max_length=200)
    description = models.TextField(_('Descrição'), max_length=5000)
    address = models.ForeignKey(Address, related_name='garages')
    service_type = models.ForeignKey(ServiceType, related_name='garages')
    tag = models.ForeignKey(Tag, related_name='garages')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Mecânica')
        verbose_name_plural = _('Mecânicas')
