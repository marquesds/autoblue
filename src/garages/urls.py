from django.conf.urls import url
from .views import garages

urlpatterns = [
    url(r'^', garages, name='garages'),
]
