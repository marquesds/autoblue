from django.contrib import admin
from .models import Vehicle


@admin.register(Vehicle)
class GarageAdmin(admin.ModelAdmin):
    search_fields = ('fipe_code', 'licence_plate', 'manufacturer', 'model', 'year_manufacture')
    ordering = ('year_manufacture',)
