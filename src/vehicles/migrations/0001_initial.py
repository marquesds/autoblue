# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-15 01:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fipe_code', models.CharField(max_length=20, verbose_name='Código Fipe')),
                ('licence_plate', models.CharField(max_length=8, verbose_name='Placa')),
                ('manufacturer', models.CharField(max_length=200, verbose_name='Fabricante')),
                ('model', models.CharField(max_length=200, verbose_name='Modelo')),
                ('year_manufacture', models.CharField(max_length=4, verbose_name='Ano de Fabricação')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'Veículos',
                'verbose_name': 'Veículo',
            },
        ),
    ]
