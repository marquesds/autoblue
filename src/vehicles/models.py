from django.db import models
from django.utils.translation import ugettext_lazy as _


class Vehicle(models.Model):
    fipe_code = models.CharField(_('Código Fipe'), max_length=20)
    licence_plate = models.CharField(_('Placa'), max_length=8)
    manufacturer = models.CharField(_('Fabricante'), max_length=200)
    model = models.CharField(_('Modelo'), max_length=200)
    year_manufacture = models.CharField(_('Ano de Fabricação'), max_length=4)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Veículo')
        verbose_name_plural = _('Veículos')
