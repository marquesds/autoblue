from django.conf.urls import url
from .views import vehicles

urlpatterns = [
    url(r'^', vehicles, name='vehicles'),
]
